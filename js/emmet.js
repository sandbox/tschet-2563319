/**
 * @file
 * JavaScript Document.
 */

jQuery(document).ready(function ($) {
    'use strict';
    var profile = Drupal.settings.emmet.profile;
    $("textarea").addClass(profile);
  }
);
